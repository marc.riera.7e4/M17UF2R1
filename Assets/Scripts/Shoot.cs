﻿using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private StarterAssetsInputs _input;
    public GameObject bullet;
    public float shootForce;

    private void Start()
    {
        _input = GetComponentInParent<StarterAssetsInputs>();
    }

    void Update()
    {
        float x = Screen.width / 2f;
        float y = Screen.height / 2f;


        if (_input.shoot && gameObject.GetComponentInParent<ThirdPersonController>().canShoot == true)
        {
            Debug.Log("shut");
            GameObject clone;
            clone = Instantiate(bullet, transform.position, new Quaternion(90, 90, GameObject.Find("Character_Explorer_Female_01").GetComponent<Transform>().rotation.y, 1));
            //clone.transform.forward = new Vector3(x, y, 10);
            clone.GetComponent<Rigidbody>().AddForce(new Vector3(x, y, 10) * shootForce);
            _input.shoot = false;
        }
    }
}
