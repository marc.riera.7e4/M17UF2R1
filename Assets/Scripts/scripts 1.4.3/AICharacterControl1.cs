using StarterAssets;
using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl1 : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for
        private Animator ani;
        public bool attackMode = true;
        public GameObject [] patrolPoints;
        public int point;
        private StarterAssetsInputs _input;
        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();
            ani = GetComponent<Animator>();
            agent.updateRotation = false;
	        agent.updatePosition = true;
            _input = GetComponent<StarterAssetsInputs>();
        }


        private void Update()
        {


            //transform.LookAt(target);
            //transform.rotation = new Quaternion(0, transform.rotation.y, 0,transform.rotation.w);
            


            if (attackMode == true)
            {
                target = GameObject.FindGameObjectWithTag("Player").transform;
                if (target != null)
                    agent.SetDestination(target.position);

                if (agent.remainingDistance > agent.stoppingDistance)
                {
                    character.Move(agent.desiredVelocity, false, false);
                    ani.SetBool("move", true);
                }
                else
                {
                    character.Move(Vector3.zero, false, false);
                    ani.SetBool("move", false);
                }

                if (agent.remainingDistance < 1)
                {
                    ani.SetTrigger("attack");
                }
            }
            else
            {
                target = patrolPoints[point].transform;
                agent.SetDestination(target.position);
                character.Move(agent.desiredVelocity, false, false);
                if (agent.remainingDistance<0.1f)
                {

                    ani.SetBool("move", false);
                    point++;
                    if (point >= patrolPoints.Length)
                    {
                        point = 0;
                    }
                }
                else
                {
                    ani.SetBool("move", true);
                }
            }
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }

       
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Player") {
                Debug.Log("Damage");
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                attackMode = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Player")
            {
                attackMode = false;
            }
        }
    }
    
}
